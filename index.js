console.log("Hello World");

/*
	1. Create variables to store to the following user details:

	-first name - String
	-last name - String
	-age - Number
	-hobbies - Array
	-work address - Object

		-The hobbies array should contain at least 3 hobbies as Strings.
		-The work address object should contain the following key-value pairs:

			houseNumber: <value>
			street: <value>
			city: <value>
			state: <value>

	Log the values of each variable to follow/mimic the output.

	Note:
		-Name your own variables but follow the conventions and best practice in naming variables.
		-You may add your own values but keep the variable names and values Safe For Work.
*/

	//Add your variables and console log for objective 1 here:




/*			
	2. Debugging Practice - Identify and implement the best practices of creating and using variables 
	   by avoiding errors and debugging the following codes:

			-Log the values of each variable to follow/mimic the output.
*/	



	let firstName = "Ken";
	console.log("First Name: "+firstName);
	let lastName = "Adams";
	console.log("Last Name: "+lastName);
	let age = 28;
	console.log("Age: "+age);
	let hobbies = ["Skydiving", "Contact sports", "Writing Poetry"];
	console.log("Hobbies: ");
	console.log(hobbies);

	let workAddress = {
		houseNumber: 143,
		street: "Taft",
		city: "Puerto Princesa City",
		state: "Province of Palawan"
	};
	console.log("Work Address: ");
	console.log(workAddress);

	// Displaying Full Name
	let fullName = firstName+' '+lastName;
	console.log("My Full name is: "+fullName);

	// Displaying my current Age
	console.log("My current Age is: "+age);

	// Listing my Friends
	let myFriends = ["Chandler", "Monica", "Phoebe", "Rachel", "Ross", "Joey"];
	console.log("My Friends are: ");
	console.log(myFriends);

	// My Full Profile
	let myProfile = {
		userName: "ken_adams@friends.io",
		fullName: "Ken Adams",
		age: 28,
		isActive: true
	}
	console.log("My Full Profile:");
	console.log(myProfile);

	// My bestfriend
	let myBestFriend = "Chandler bing";
	console.log("My bestfriend is: "+myBestFriend);

	// My present location
	const lastLocation = ["Rio Tuba, Palawan"];
	lastLocation[0] = "Puerto Princesa City, Palawan";
	console.log("My present location is: "+lastLocation);

	// Favourite line
	let favouriteLine = "How you doing? ;-)";
	console.log("A one liner for a girl: "+favouriteLine);